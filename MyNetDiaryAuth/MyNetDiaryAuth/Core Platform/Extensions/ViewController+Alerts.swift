//
//  ViewController+Alerts.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String? = nil, text: String? = nil, handler: (()->())? = nil) {
        
        let alertController = UIAlertController(title: title,
                                                message: text,
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel) { _ in
            handler?()
        })
        present(alertController, animated: true)
    }
}

//
//  String+Trim.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/29/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

extension String {
    func trimTrailingZeros() -> String {
        var parts = self.split(separator: ".").map(String.init)
        if parts.count > 1 {
            var lastPart = parts.removeLast()
            lastPart = lastPart.replacingOccurrences(of: "0*$", with: "", options: .regularExpression)
            
            if !lastPart.isEmpty {
                parts.append(lastPart)
            }
        }
        return parts.joined(separator: ".")
    }
}

//
//  BaseCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

open class BaseCoordinator: Coordinator {
    
    public private(set) var childCoordinators: [Coordinator] = []
    public private(set) weak var parentController: UIViewController?
    public weak var viewController: UIViewController?
    public weak var delegate: CoordinatorDelegate?
    
    public let context: AppContext
    
    public init(context: AppContext) {
        self.context = context
    }
    
    public init(parentController: UIViewController, context: AppContext) {
        self.parentController = parentController
        self.context = context
    }
    
    public func instantiateViewController() -> UIViewController? {
        nil
    }
    
    open func start(animated: Bool) {
        guard delegate?.canShowCoordinator(self) != false,
            let viewController = instantiateViewController() else { return }
        
        self.viewController = viewController
        
        switch parentController {
        case let navigationController as UINavigationController:
            navigationController.pushViewController(viewController, animated: animated)
            
        default:
            parentController?.present(viewController, animated: animated)
        }
        (viewController as? BaseViewController)?.coordinatorDelegate = self
        delegate?.coordinatorDidShow(self)
    }
    
    public func present(animated: Bool) {
        guard delegate?.canShowCoordinator(self) != false,
            let viewController = instantiateViewController() else { return }
        
        self.viewController = viewController
        
        parentController?.present(viewController, animated: animated)
        (viewController as? BaseViewController)?.coordinatorDelegate = self
        delegate?.coordinatorDidShow(self)
    }
    
    open func end(animated: Bool) {
        viewController = nil
        delegate?.coordinatorDidClose(self)
    }
}

extension BaseCoordinator: CoordinatorDelegate {
    public func coordinatorDidClose(_ coordinator: Coordinator) {
        childCoordinators.removeAll(where: { $0 === coordinator })
    }
    
    public func coordinatorDidShow(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }
}

extension BaseCoordinator: BaseViewControllerCoordinatorDelegate {
    func viewControllerDidDisappear() {
        end(animated: false)
    }
}

//
//  Coordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

public protocol CoordinatorDelegate: class {
    func canShowCoordinator(_ coordinator: Coordinator) -> Bool
    func coordinatorDidShow(_ coordinator: Coordinator)
    func coordinatorDidClose(_ coordinator: Coordinator)
}

public extension CoordinatorDelegate where Self: Coordinator {
    func canShowCoordinator(_ coordinator: Coordinator) -> Bool {
        coordinator.viewController == nil &&
            !childCoordinators.contains(where: { $0 === coordinator })
    }
}

public protocol Coordinator: class {
    
    var childCoordinators: [Coordinator] { get }
    var viewController: UIViewController? { get }
    var delegate: CoordinatorDelegate? { get set }
    
    func start(animated: Bool)
    func present(animated: Bool)
    func end(animated: Bool)
    
    func instantiateViewController() -> UIViewController?
}

public extension Coordinator {
    func end(animated: Bool) {
        viewController?.dismiss(animated: animated)
        delegate?.coordinatorDidClose(self)
    }
}

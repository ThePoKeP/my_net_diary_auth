//
//  CaloriesFormatter.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/29/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import Foundation

class CaloriesFormatter {

    private static let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        formatter.roundingMode = .up
        return formatter
    }()

    public static func string(for dailyCaloriesBudget: Double) -> String? {
        numberFormatter.string(for: dailyCaloriesBudget)
    }

}

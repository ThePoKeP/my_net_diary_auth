//
//  DailyCalorieBudgetCalculator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

final class DailyCalorieBudgetCalculator {
    
    static func calculate(for profile: BaseProfile) -> Double {
        
        let weight = profile.weight.value(for: .kg)
        let height = profile.height.value(for: .meter)
        let age = Double(AgeCalculator.calculate(with: profile.birthday))
        let pa = profile.pa.rawValue
        
        switch profile.gender {
        case .male:
            return 662.0 - 9.53 * age +
                pa * (15.91 * weight + 539.6 * height)
            
        case .female:
            return 354 - 6.91 * age +
                pa * (9.36 * weight + 726 * height)
        }
    }
    
}

//
//  AgeCalculator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class AgeCalculator {
    
    static func calculate(with birthday: Date) -> Int {
        let ageComponents = Calendar.current.dateComponents([.year],
                                                            from: birthday,
                                                            to: Date())
        let age = ageComponents.year
        return age ?? 0
    }
    
    static func date(for age: UInt) -> Date? {
        Calendar.current.date(byAdding: .year, value: -Int(age), to: Date())
    }
}

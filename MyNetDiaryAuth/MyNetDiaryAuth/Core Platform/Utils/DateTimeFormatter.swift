//
//  DateTimeFormatter.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class DateTimeFormatter {
    
    static var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter
    }()
    
    static public func birthdayString(from date: Date) -> String {
        
        dateFormatter.dateFormat = "MMM d, YYYY"
        return dateFormatter.string(from: date)
    }
}

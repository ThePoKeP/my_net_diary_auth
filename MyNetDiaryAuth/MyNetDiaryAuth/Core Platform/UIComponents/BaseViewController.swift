//
//  BaseViewController.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

protocol BaseViewControllerCoordinatorDelegate: class {
    func viewControllerDidDisappear()
}

class BaseViewController: UIViewController {
    
    fileprivate let DefaultKeyboardAnimationDuration: Double = 0.25
    
    weak var coordinatorDelegate: BaseViewControllerCoordinatorDelegate?
    
    fileprivate var isAnimating: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    deinit {
        coordinatorDelegate?.viewControllerDidDisappear()
        unsubscribeFromKeyboardNotifications()
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribeToKeyboardNotifications()
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        unsubscribeFromKeyboardNotifications()
    }
    
    func keyboardWillShow(keyboardHeight: CGFloat) { }
    func keyboardDidShow() { }
    
    func keyboardWillHide(keyboardHeight: CGFloat) { }
    func keyboardDidHide() { }
}

extension BaseViewController {
    
    fileprivate func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)),
                                               name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(_:)),
                                               name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    fileprivate func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc fileprivate func keyboardWillShow(_ notification: Notification) {
        
        guard var keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height else { return }
        keyboardHeight -= view.safeAreaInsets.bottom
        keyboardHeight += additionalSafeAreaInsets.bottom
        
        keyboardWillShow(keyboardHeight: keyboardHeight)

        let animationDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? DefaultKeyboardAnimationDuration
        
        guard additionalSafeAreaInsets.bottom != keyboardHeight, !isAnimating else { return }
        additionalSafeAreaInsets.bottom = keyboardHeight
        isAnimating = true
        
        UIView.animate(withDuration: animationDuration,
                       delay: 0,
                       options: .allowAnimatedContent,
                       animations: {
            self.view.layoutIfNeeded()
        }, completion: { [weak self] _ in
            self?.isAnimating = false
        })
    }
    
    @objc fileprivate func keyboardDidShow(_ notification: Notification) {
        keyboardDidShow()
    }
    
    @objc fileprivate func keyboardWillHide(_ notification: Notification) {
        var keyboardHeight: CGFloat = 0
        if var height = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect)?.height {
            height -= view.safeAreaInsets.bottom
            keyboardHeight = height
        }
        keyboardHeight += additionalSafeAreaInsets.bottom
        
        keyboardWillHide(keyboardHeight: keyboardHeight)
        
        let animationDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? DefaultKeyboardAnimationDuration
        
        guard additionalSafeAreaInsets.bottom > 0, !isAnimating else { return }
        
        additionalSafeAreaInsets.bottom = 0
        isAnimating = true
                          
        UIView.animate(withDuration: animationDuration,
                       delay: 0,
                       options: .allowAnimatedContent,
                       animations: {
            self.view.layoutIfNeeded()
        }, completion: { [weak self] _ in
            self?.isAnimating = false
        })
    }
    
    @objc fileprivate func keyboardDidHide(_ notification: Notification) {
        keyboardDidHide()
    }
}

//
//  BodyHeight.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/29/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import HealthKit

public enum BodyHeightUnitType: HKUnitProvider {
    
    case meter
    case foot
    
    public var hkUnit: HKUnit {
        switch self {
        case .meter:
            return HKUnit.meter()
            
        case .foot:
            return HKUnit.foot()
        }
    }
}

public typealias BodyHeight = BodyParameter<BodyHeightUnitType>

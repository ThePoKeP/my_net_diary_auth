//
//  LifestyleActivity.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

public enum LifestyleActivity: Double {
    case low = 0.5
    case normal = 1.0
    case hight = 1.5
}

//
//  BaseProfile.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import Foundation

public struct BaseProfile {
    let birthday: Date
    let gender: Gender
    let pa: LifestyleActivity
    let height: BodyHeight
    let weight: BodyWeight
}

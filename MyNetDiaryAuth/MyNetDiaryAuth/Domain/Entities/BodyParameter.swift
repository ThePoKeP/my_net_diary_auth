//
//  BodyParameter.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/29/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import HealthKit

public protocol HKUnitProvider {
    var hkUnit: HKUnit { get }
}

public struct BodyParameter<T> where T: HKUnitProvider {
    
    private let quantity: HKQuantity
    var unitType: T
    
    var value: Double {
        quantity.doubleValue(for: unitType.hkUnit)
    }
    
    func value(for unitType: T) -> Double {
        quantity.doubleValue(for: unitType.hkUnit)
    }
    
    func stringValue(for unitType: T) -> String {
        "\(Int(quantity.doubleValue(for: unitType.hkUnit))) \(unitType.hkUnit.unitString)"
    }
    
    init(with value: Double, unitType: T) {
        self.unitType = unitType
        self.quantity = HKQuantity(unit: unitType.hkUnit, doubleValue: value)
    }
}

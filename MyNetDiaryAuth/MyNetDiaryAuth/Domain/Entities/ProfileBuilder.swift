//
//  ProfileBuilder.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/29/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

public protocol ProfileBuilder {
    
    var weight: BodyWeight? { get }
    var height: BodyHeight? { get }
    var birthday: Date? { get }
    var gender: Gender? { get }
    var pa: LifestyleActivity? { get }
    
    func fillWeight(_ weight: BodyWeight)
    func fillHeight(_ height: BodyHeight)
    func fillBirthday(_ birthday: Date)
    func fillPA(_ pa: LifestyleActivity)
    func fillGender(_ gender: Gender)
    
    func reset()
    func build() -> BaseProfile?
    
}

class ProfileBuilderImpl: ProfileBuilder {
    
    private let defaultHeight: BodyHeight = BodyHeight(with: 1.7, unitType: .meter)
    private let defaultPA: LifestyleActivity = .normal
    
    private(set) var weight: BodyWeight?
    
    private(set) var height: BodyHeight?
    
    private(set) var birthday: Date?
    
    private(set) var gender: Gender?
    
    private(set) var pa: LifestyleActivity?
    
    init() {
        reset()
    }
    
    func fillWeight(_ weight: BodyWeight) {
        self.weight = weight
    }
    
    func fillHeight(_ height: BodyHeight) {
        self.height = height
    }
    
    func fillBirthday(_ birthday: Date) {
        self.birthday = birthday
    }
    
    func fillPA(_ pa: LifestyleActivity) {
        self.pa = pa
    }
    
    func fillGender(_ gender: Gender) {
        self.gender = gender
    }
    
    func reset() {
        height = defaultHeight
        pa = defaultPA
        weight = nil
        gender = nil
        birthday = nil
        
    }
    
    func build() -> BaseProfile? {
        guard let birthday = birthday,
            let gender = gender,
            let pa = pa,
            let height = height,
            let weight = weight else {
                return nil
        }
        
        return BaseProfile(birthday: birthday,
                           gender: gender,
                           pa: pa,
                           height: height,
                           weight: weight)
    }
}

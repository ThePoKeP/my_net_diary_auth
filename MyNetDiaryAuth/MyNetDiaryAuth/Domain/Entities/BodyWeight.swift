//
//  BodyWeight.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/29/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import HealthKit

public enum BodyWeightUnitType: HKUnitProvider {
    case kg
    case pound
    
    public var hkUnit: HKUnit {
        switch self {
        case .kg:
            return HKUnit.gramUnit(with: .kilo)
            
        case .pound:
            return HKUnit.pound()
        }
    }
}

public typealias BodyWeight = BodyParameter<BodyWeightUnitType>

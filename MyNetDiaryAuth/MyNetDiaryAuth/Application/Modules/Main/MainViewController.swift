//
//  MainViewController.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController, Storyboarded {
    
    var viewModel: MainViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction private func loginButtonDidTap(sender: UIButton) {
        viewModel?.authorize()
    }
}

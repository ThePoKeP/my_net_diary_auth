//
//  MainCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class MainCoordinator: BaseCoordinator {
    
    private weak var window: UIWindow?
    
    init(with window: UIWindow?, context: AppContext) {
        self.window = window
        super.init(context: context)
    }
    
    override func instantiateViewController() -> UIViewController? {
    
        let mainViewController = MainViewController.instantiate()
        let mainViewModel = MainViewModelImpl()
        mainViewModel.coordinatorDelegate = self
        mainViewController.viewModel = mainViewModel
        
        return mainViewController
    }

    override func start(animated: Bool) {
        self.viewController = instantiateViewController()
        window?.rootViewController = viewController
    }
}

extension MainCoordinator: MainViewModelCoordinatorDelegate {
    
    func viewModelDidstartAuthorization() {
        guard let parentController = viewController else { return }
        let signUpCoordinator = AuthCoordinator(parentController: parentController,
                                                context: context)
        signUpCoordinator.delegate = self
        signUpCoordinator.start(animated: true)
    }
}

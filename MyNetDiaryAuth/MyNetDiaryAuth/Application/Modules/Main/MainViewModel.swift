//
//  MainViewModel.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

protocol MainViewModelCoordinatorDelegate: class {
    func viewModelDidstartAuthorization()
}

protocol MainViewModel: ViewModel {
    func authorize()
}

class MainViewModelImpl: MainViewModel {
    
    weak var coordinatorDelegate: MainViewModelCoordinatorDelegate?
    
    func authorize() {
        coordinatorDelegate?.viewModelDidstartAuthorization()
    }
}

//
//  BirthdayStepViewController.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class BirthdayStepViewController: BaseViewController {

    @IBOutlet private weak var birthdayButton: UIButton!
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private var birthdayPicker: UIDatePicker!
    
    var viewModel: BirthdayStepViewModel? {
        didSet {
            oldValue?.delegate = nil
            bindViewModel()
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        true
    }
    
    override var inputView: UIView? {
        birthdayPicker
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        becomeFirstResponder()
        title = "When You Were Born?"
        
        bindViewModel()
    }
    
    func bindViewModel() {
        guard isViewLoaded,
            let viewModel = viewModel else { return }
        
        viewModel.delegate = self
        birthdayPicker.minimumDate = viewModel.minDate
        birthdayPicker.maximumDate = viewModel.maxDate
        birthdayPicker.setDate(viewModel.birthday, animated: false)
        birthdayButton.setTitle(viewModel.birthdayString, for: .normal)
    }
    
    @IBAction private func birthdayButtonDidTap(sender: UIButton) {
        if isFirstResponder {
            resignFirstResponder()
            
        } else {
            becomeFirstResponder()
        }
    }

    @IBAction private func nextButtondDidTap(sender: UIButton) {
        viewModel?.saveBirthday()
    }
    
    @IBAction private func birthdayPickerValueDidChange(sender: UIDatePicker) {
        do {
            if try viewModel?.validateBirthday(sender.date) == true {
                viewModel?.birthday = sender.date
            }
        } catch BirthdayError.tooYoung {
            showAlert(title: "Sorry", text: "You are to young yet. Min age is 13 years old")
        } catch {}
    }
}

extension BirthdayStepViewController: BirthdayStepViewModelDelegate {
    
    func viewModel(viewModel: BirthdayStepViewModel, birthdayDidChange birthday: Date) {
        birthdayButton.setTitle(viewModel.birthdayString, for: .normal)
    }
}

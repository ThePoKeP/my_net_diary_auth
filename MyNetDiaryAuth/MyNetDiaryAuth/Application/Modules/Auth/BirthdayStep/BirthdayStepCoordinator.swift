//
//  BirthdayStepCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class BirthdayStepCoordinator: BaseCoordinator {
    
    weak var resultDelegate: SignUpStepCoordinatorResultDelegate?
    
    override func instantiateViewController() -> UIViewController? {
        let birthdayStepViewController = BirthdayStepViewController()
        let birthdayStepViewModel = BirthdayStepViewModelImpl(profileBuilder: context.profileBuilder)
        birthdayStepViewController.viewModel = birthdayStepViewModel
        birthdayStepViewModel.coordinatorDelegate = self
        
        return birthdayStepViewController
    }
}

extension BirthdayStepCoordinator: BirthdayStepViewModelCoordinatorDelegate {
    
    func viewModelDidFillBirthday() {
        resultDelegate?.signUpCoordinatorDidCompleteStep(self)
    }
}

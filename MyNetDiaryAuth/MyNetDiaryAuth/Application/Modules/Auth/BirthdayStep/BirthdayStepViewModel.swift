//
//  BirthdaySterpViewModel.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import Foundation

enum BirthdayError: Error {
    case tooYoung
}

protocol BirthdayStepViewModelCoordinatorDelegate: class {
    func viewModelDidFillBirthday()
}

protocol BirthdayStepViewModelDelegate: class {
    func viewModel(viewModel: BirthdayStepViewModel, birthdayDidChange birthday: Date)
}

protocol BirthdayStepViewModel: ViewModel {
    
    var delegate: BirthdayStepViewModelDelegate? { get set }
    
    var birthday: Date { get set }
    var birthdayString: String { get }
    var minDate: Date? { get }
    var maxDate: Date? { get }
    
    func validateBirthday(_ birthday: Date) throws -> Bool
    func saveBirthday()
}

class BirthdayStepViewModelImpl: BirthdayStepViewModel {
    
    weak var delegate: BirthdayStepViewModelDelegate?
    
    weak var coordinatorDelegate: BirthdayStepViewModelCoordinatorDelegate?
    
    var birthday: Date = Date() {
        didSet {
            updateBirthdayString()
            delegate?.viewModel(viewModel: self, birthdayDidChange: birthday)
        }
    }
    
    private(set) var birthdayString: String = ""
    
    let minDate: Date?
    let maxDate: Date?
    
    private let profileBuilder: ProfileBuilder
    
    init(profileBuilder: ProfileBuilder) {
        
        self.profileBuilder = profileBuilder
        
        minDate = AgeCalculator.date(for: 100)
        maxDate = AgeCalculator.date(for: 13)
        
        birthday = profileBuilder.birthday ?? minDate ?? maxDate ?? Date()
        updateBirthdayString()
    }
    
    private func updateBirthdayString() {
        birthdayString = DateTimeFormatter.birthdayString(from: birthday).uppercased()
    }
    
    func validateBirthday(_ birthday: Date) throws -> Bool {
        let age = AgeCalculator.calculate(with: birthday)
        if age < 13 {
            throw BirthdayError.tooYoung
        }
        return true
    }
    
    func saveBirthday() {
        profileBuilder.fillBirthday(birthday)
        coordinatorDelegate?.viewModelDidFillBirthday()
    }
}

//
//  DailyCaloriesBudgetStepViewModel.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

protocol DailyCaloriesBudgetStepViewModel: ViewModel {

    var result: String? { get }
}

class DailyCaloriesBudgetStepViewModelImpl: DailyCaloriesBudgetStepViewModel {
    
    let result: String?
    
    private let profileBuilder: ProfileBuilder
    
    init(profileBuilder: ProfileBuilder) {
        self.profileBuilder = profileBuilder
        if let profile = profileBuilder.build() {
            let budget = DailyCalorieBudgetCalculator.calculate(for: profile)
            result = CaloriesFormatter.string(for: budget)
        } else {
            result = "Something went wrong:("
        }
    }
}

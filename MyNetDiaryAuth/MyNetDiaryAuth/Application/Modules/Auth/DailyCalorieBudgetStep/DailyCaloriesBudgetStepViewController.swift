//
//  DailyCaloriesBudgetStepViewController.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class DailyCaloriesBudgetStepViewController: BaseViewController {
    
    @IBOutlet private weak var resultLabel: UILabel!
    
    var viewModel: DailyCaloriesBudgetStepViewModel? {
        didSet {
            bindViewModel()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Daily Calories Budget"

        bindViewModel()
    }
    
    private func bindViewModel() {
        guard isViewLoaded else { return }
        
        resultLabel.text = viewModel?.result
    }
}

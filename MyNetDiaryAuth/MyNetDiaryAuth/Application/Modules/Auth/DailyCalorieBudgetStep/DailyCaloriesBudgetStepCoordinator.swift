//
//  DailyCaloriesBudgetStepCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class DailyCaloriesBudgetStepCoordinator: BaseCoordinator {

    override func instantiateViewController() -> UIViewController? {
        let dailyCaloriesBudgetStepViewController = DailyCaloriesBudgetStepViewController()
        let dailyCaloriesBudgetStepViewModel = DailyCaloriesBudgetStepViewModelImpl(profileBuilder: context.profileBuilder)
        dailyCaloriesBudgetStepViewController.viewModel = dailyCaloriesBudgetStepViewModel
        
        return dailyCaloriesBudgetStepViewController
    }
}

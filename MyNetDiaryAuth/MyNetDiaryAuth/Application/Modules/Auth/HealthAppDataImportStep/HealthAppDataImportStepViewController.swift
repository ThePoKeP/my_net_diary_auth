//
//  HealthAppDataImportStepViewController.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class HealthAppDataImportStepViewController: BaseViewController {
    
    var viewModel: HealthAppDataImportStepViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Health App Import"
    }
    
    
    @IBAction private func importButtonDidTap(sender: UIButton) {
        viewModel?.importDataFromHeathApp()
    }
    
    @IBAction private func skipButtonDidTap(sender: UIButton) {
        viewModel?.skipDataImport()
    }

}

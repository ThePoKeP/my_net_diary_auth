//
//  HealthAppDataImportStepViewModel.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import HealthKit

protocol HealthAppDataImportStepViewModelCoordinatorDeleagte: class {
    func dataImportDidSkip()
    func dataImportDidComplete()
}

protocol HealthAppDataImportStepViewModel: ViewModel {
    
    func skipDataImport()
    func importDataFromHeathApp()
}

class HealthAppDataImportStepViewModelImpl: HealthAppDataImportStepViewModel {
    
    weak var coordinatorDelegate: HealthAppDataImportStepViewModelCoordinatorDeleagte?
    
    private let healthStoreService: HealthStoreService
    
    private let profileBuilder: ProfileBuilder
    
    
    init(healthStoreService: HealthStoreService,
         profileBuilder: ProfileBuilder) {
        self.healthStoreService = healthStoreService
        self.profileBuilder = profileBuilder
    }
    
    func skipDataImport() {
        coordinatorDelegate?.dataImportDidSkip()
    }
    
    func importDataFromHeathApp() {
        requestPermissions { [weak self] success in
            
            self?.requestBirthday { birthday in
                if let birthday = birthday {
                    self?.profileBuilder.fillBirthday(birthday)
                }
            }
            
            self?.requestHeight { height in
                if let height = height {
                    self?.profileBuilder.fillHeight(height)
                }
            }
            
            self?.requestWeight { weight in
                if let weight = weight {
                    self?.profileBuilder.fillWeight(weight)
                    self?.coordinatorDelegate?.dataImportDidComplete()
                }
            }
        }
    }
    
    
    
    private func requestPermissions(completionHandler: @escaping (Bool) -> Void) {
        healthStoreService.requestAuthorization { result in
            switch result {
            case .success(let status):
                completionHandler(status)
                
            case .fail:
                completionHandler(false)
            }
        }
    }
    
    private func requestBirthday(completionHandler: @escaping (Date?) -> Void) {
        healthStoreService.requestBirthday { result in
            switch result {
            case .success(let birthday):
                completionHandler(birthday)
                
            case .fail:
                completionHandler(nil)
            }
        }
    }
    
    private func requestHeight(completionHandler: @escaping (BodyHeight?) -> Void) {
        healthStoreService.requestCurrentHeight { result in
            switch result {
            case .success(let height):
                completionHandler(height)
                
            case .fail:
                completionHandler(nil)
            }
        }
    }
    
    private func requestWeight(completionHandler: @escaping (BodyWeight?) -> Void) {
        healthStoreService.requestCurrentWeight { result in
            switch result {
            case .success(let weight):
                completionHandler(weight)
                
            case .fail:
                completionHandler(nil)
            }
        }
    }
}

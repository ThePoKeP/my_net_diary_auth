//
//  HealthAppDataImportStepCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class HealthAppDataImportStepCoordinator: BaseCoordinator {
    
    weak var resultDelegate: SignUpStepCoordinatorResultDelegate?
    
    override func instantiateViewController() -> UIViewController? {
        let healthAppDataImportStepViewController = HealthAppDataImportStepViewController()
        let healthAppDataImportStepViewModel = HealthAppDataImportStepViewModelImpl(healthStoreService: context.healthStoreService,
                                                                                    profileBuilder: context.profileBuilder)
        healthAppDataImportStepViewController.viewModel = healthAppDataImportStepViewModel
        healthAppDataImportStepViewModel.coordinatorDelegate = self
        
        return healthAppDataImportStepViewController
    }
}

extension HealthAppDataImportStepCoordinator: HealthAppDataImportStepViewModelCoordinatorDeleagte {
    
    func dataImportDidSkip() {
        resultDelegate?.signUpCoordinatorDidCompleteStep(self)
    }
    
    func dataImportDidComplete() {
        resultDelegate?.signUpCoordinatorDidCompleteStep(self)
    }
}

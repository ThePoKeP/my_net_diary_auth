//
//  AuthCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class AuthCoordinator: BaseCoordinator {
    
    override func instantiateViewController() -> UIViewController? {
        
        let mainAuthViewController = UINavigationController()
        mainAuthViewController.navigationBar.backgroundColor = .clear
        mainAuthViewController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        mainAuthViewController.navigationBar.shadowImage = UIImage()
        mainAuthViewController.navigationBar.tintColor = UIColor(named: "tint_color")
        
        let startAuthStepCoordinator = StartAuthStepCoordinator(parentController: mainAuthViewController,
                                                                context: context)
        startAuthStepCoordinator.delegate = self
        startAuthStepCoordinator.resultDelegate = self
        
        startAuthStepCoordinator.start(animated: false)
        
        return mainAuthViewController
    }
    
    private func goToSelectGenderStep() {
        guard let parentViewController = viewController else { return }
        
        let selectGenderStepCoordinator = SelectGenderStepCoordinator(parentController: parentViewController,
                                                                      context: context)
        selectGenderStepCoordinator.delegate = self
        selectGenderStepCoordinator.resultDelegate = self
        selectGenderStepCoordinator.start(animated: true)
    }
    
    private func goToHealthAppDataImportStep() {
        guard let parentViewController = viewController else { return }
        
        let healthAppDataImportStepCoordinator = HealthAppDataImportStepCoordinator(parentController: parentViewController,
                                                                                    context: context)
        healthAppDataImportStepCoordinator.delegate = self
        healthAppDataImportStepCoordinator.resultDelegate = self
        healthAppDataImportStepCoordinator.start(animated: true)
    }
    
    private func goToBithdayStep() {
        guard let parentViewController = viewController else { return }
        
        let bigthdayStepCoordinator = BirthdayStepCoordinator(parentController: parentViewController,
                                                              context: context)
        bigthdayStepCoordinator.delegate = self
        bigthdayStepCoordinator.resultDelegate = self
        bigthdayStepCoordinator.start(animated: true)
    }
    
    private func goToWeightStep() {
        guard let parentViewController = viewController else { return }
        
        let weightStepCoordinator = WeightStepCoordinator(parentController: parentViewController,
                                                          context: context)
        weightStepCoordinator.delegate = self
        weightStepCoordinator.resultDelegate = self
        weightStepCoordinator.start(animated: true)
    }
    
    private func goToDailyCaloriesBudget() {
        guard let parentViewController = viewController else { return }
        
        let weightStepCoordinator = DailyCaloriesBudgetStepCoordinator(parentController: parentViewController,
                                                                       context: context)
        weightStepCoordinator.delegate = self
        weightStepCoordinator.start(animated: true)
    }
}

extension AuthCoordinator: StartAuthStepCoordinatorResultDelegate {

    func goToSignIn() {
        //TODO:
        viewController?.showAlert(title: "Not implemented yet")
    }
    
    func goToSignUp() {
        goToSelectGenderStep()
    }
}

extension AuthCoordinator: SignUpStepCoordinatorResultDelegate {
    func signUpCoordinatorDidCompleteStep(_ coordinator: Coordinator) {
        switch coordinator {
            
        case is SelectGenderStepCoordinator:
            goToHealthAppDataImportStep()
            
        case is HealthAppDataImportStepCoordinator:
            goToWeightStep()
                
        case is WeightStepCoordinator:
            goToBithdayStep()
            
        case is BirthdayStepCoordinator:
            goToDailyCaloriesBudget()
            
        default:
            break
        }
    }
}

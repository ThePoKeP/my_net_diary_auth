//
//  StartAuthStepCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

protocol StartAuthStepCoordinatorResultDelegate: class {
    func goToSignIn()
    func goToSignUp()
}

class StartAuthStepCoordinator: BaseCoordinator {
    
    weak var resultDelegate: StartAuthStepCoordinatorResultDelegate?

    override func instantiateViewController() -> UIViewController? {
        
        let startAuthStepViewController = StartAuthStepViewController()
        let startAuthStepViewModel = StartAuthStepViewModelImpl()
        startAuthStepViewModel.coordinatorDelegate = self
        startAuthStepViewController.viewModel = startAuthStepViewModel
        
        return startAuthStepViewController
    }
}

extension StartAuthStepCoordinator: StartAuthStepViewModelCoordinatorDelegate {
    
    func viewModelDidStartSignIn() {
        resultDelegate?.goToSignIn()
    }
    
    func viewModelDidStartSignUp() {
        resultDelegate?.goToSignUp()
    }
    
}

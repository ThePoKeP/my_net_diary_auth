//
//  StartAuthStepViewModel.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

protocol StartAuthStepViewModelCoordinatorDelegate: class {
    func viewModelDidStartSignUp()
    func viewModelDidStartSignIn()
}

protocol StartAuthStepViewModel: ViewModel {
    func signUp()
    func signIn()
}

class StartAuthStepViewModelImpl: StartAuthStepViewModel {
    
    weak var coordinatorDelegate: StartAuthStepViewModelCoordinatorDelegate?
    
    func signUp() {
        coordinatorDelegate?.viewModelDidStartSignUp()
    }
    
    func signIn() {
        coordinatorDelegate?.viewModelDidStartSignIn()
    }
}

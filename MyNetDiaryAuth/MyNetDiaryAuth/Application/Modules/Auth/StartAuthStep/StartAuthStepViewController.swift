//
//  StartAuthStepViewController.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class StartAuthStepViewController: BaseViewController {
    
    @IBOutlet private weak var signUpButton: UIButton!
    @IBOutlet private weak var signInButton: UIButton!

    init() {
        super.init(nibName: String(describing: Self.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    var viewModel: StartAuthStepViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction private func singUpButtonDidTap(sender: UIButton) {
        viewModel?.signUp()
    }
    
    @IBAction private func singInButtonDidTap(sender: UIButton) {
        viewModel?.signIn()
    }
}

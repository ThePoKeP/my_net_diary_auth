//
//  WeightStepCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class WeightStepCoordinator: BaseCoordinator {
    
    weak var resultDelegate: SignUpStepCoordinatorResultDelegate?

    override func instantiateViewController() -> UIViewController? {
        let weightStepViewController = WeightStepViewController()
        let weightStepViewModel = WeightStepViewModelImpl(profileBuilder: context.profileBuilder)
        weightStepViewController.viewModel = weightStepViewModel
        weightStepViewModel.coordinatorDelegate = self
        
        return weightStepViewController
    }
}

extension WeightStepCoordinator: WeightStepViewModelCoordinatorDelegate {
    
    func viewModelDidEnterWeight() {
        resultDelegate?.signUpCoordinatorDidCompleteStep(self)
    }
}

//
//  WeightStepViewModel.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import Foundation

private let minWeight = BodyWeight(with: 20, unitType: .kg)
private let maxWeight = BodyWeight(with: 440, unitType: .kg)

protocol WeightStepViewModelCoordinatorDelegate: class {
    func viewModelDidEnterWeight()
}

protocol WeightStepViewModelDelegate: class {
    func viewModel(_ viewModel: WeightStepViewModel, weightDidChange weight: Double?)
    func viewModel(_ viewModel: WeightStepViewModel, usingMetricSystemDidChange useMetricSystem: Bool)
}

enum WeightError: Error {
    case empty
    case tooLow(String)
    case tooHigh(String)
    
    var message: String {
        switch self {
        case .empty:
            return "Please enter weight"
            
        case .tooLow(let weightPart):
            return "Minimum weight supported by MyNetDiary is \(weightPart)."
            
        case .tooHigh(let weightPart):
            return "Maximum weight supported by MyNetDiary is \(weightPart)."
        }
    }
}

protocol WeightStepViewModel: ViewModel {
    
    var delegate: WeightStepViewModelDelegate? { get set }
    
    var weightPlaceholder: String { get }
    var weightPrompt: String { get }
    
    var weight: Double? { get set }
    var weightString: String? { get }
    var useMetricSystem: Bool { get set }
    
    func saveWeigth() throws
}

class WeightStepViewModelImpl: WeightStepViewModel {
    
    weak var coordinatorDelegate: WeightStepViewModelCoordinatorDelegate?
    weak var delegate: WeightStepViewModelDelegate?
    
    var weightPlaceholder: String {
        useMetricSystem ? "kg" : "pounds"
    }
    
    var weightPrompt: String {
        useMetricSystem ? "Enter your current weught in kg" : "Enter your current weight in pounds"
    }
    
    var weight: Double? {
        didSet {
            delegate?.viewModel(self, weightDidChange: weight)
        }
    }
    
    var weightString: String? {
        guard let weight = weight else { return nil }
        return String(format: "%f", weight).trimTrailingZeros()
    }
    
    var useMetricSystem: Bool {
        didSet {
            delegate?.viewModel(self, usingMetricSystemDidChange: useMetricSystem)
        }
    }
    
    private var unitType: BodyWeightUnitType {
        useMetricSystem ? .kg : .pound
    }
    
    private let profileBuilder: ProfileBuilder
    
    init(profileBuilder: ProfileBuilder) {
        self.profileBuilder = profileBuilder
        useMetricSystem = Locale.current.currencyCode == "en-us"
        self.weight = profileBuilder.weight?.value(for: unitType).rounded()
    }
    
    func saveWeigth() throws {
        let weight = try validateWeigth()
        profileBuilder.fillWeight(BodyWeight(with: weight, unitType: unitType))
        coordinatorDelegate?.viewModelDidEnterWeight()
    }
    
    private func validateWeigth() throws -> Double {
        guard let weight = weight else { throw WeightError.empty }
        switch weight {
        case maxWeight.value(for: unitType)...:
            let weightPart = maxWeight.stringValue(for: useMetricSystem ? .kg : .pound)
            throw WeightError.tooHigh(weightPart)
            
        case 0..<minWeight.value(for: unitType):
            let weightPart = minWeight.stringValue(for: useMetricSystem ? .kg : .pound)
            throw WeightError.tooLow(weightPart)
            
        default:
            return weight
            
        }
    }
}

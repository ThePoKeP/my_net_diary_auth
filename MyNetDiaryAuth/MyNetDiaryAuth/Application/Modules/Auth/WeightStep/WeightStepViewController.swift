//
//  WeightStepViewController.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class WeightStepViewController: BaseViewController {

    @IBOutlet private weak var weightTextField: UITextField!
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var useMetricSystemSwitch: UISwitch!
    @IBOutlet private weak var promptLabel: UILabel!
    
    
    var viewModel: WeightStepViewModel? {
        didSet {
            oldValue?.delegate = nil
            bindViewModel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Your Current Weight"
        
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        weightTextField.becomeFirstResponder()
    }
    
    private func bindViewModel() {
        guard isViewLoaded,
            let viewModel = viewModel else { return }
        
        viewModel.delegate = self
        
        useMetricSystemSwitch.isOn = viewModel.useMetricSystem
        weightTextField.text = viewModel.weightString
        updateWeightTexts()
    }
    
    private func updateWeightTexts() {
        weightTextField.placeholder = viewModel?.weightPlaceholder
        promptLabel.text = viewModel?.weightPrompt
    }
    
    @IBAction private func nextButtonDidTap(sender: UIButton) {
        viewModel?.weight = Double(weightTextField.text ?? "0") ?? 0
        do {
            try viewModel?.saveWeigth()
            
        } catch let error as WeightError {
            showAlert(title: error.message)
            
        }  catch {}
    }
    
    @IBAction private func useMetricSystemSitchValueDidChange(sender: UISwitch) {
        viewModel?.useMetricSystem = sender.isOn
    }
}

extension WeightStepViewController: WeightStepViewModelDelegate {
    
    func viewModel(_ viewModel: WeightStepViewModel, weightDidChange weight: Double?) {
        
    }
    
    func viewModel(_ viewModel: WeightStepViewModel, usingMetricSystemDidChange useMetricSystem: Bool) {
        updateWeightTexts()
    }
    
}


extension WeightStepViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let text: NSString = (textField.text ?? "") as NSString
        let result: NSString = text.replacingCharacters(in: range, with: string) as NSString
        return result == "" || result.doubleValue > 0 || result == "0"
    }
}

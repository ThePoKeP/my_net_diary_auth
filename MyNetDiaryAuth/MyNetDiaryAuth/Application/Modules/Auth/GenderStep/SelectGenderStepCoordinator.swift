//
//  SelectGenderStepCoordinator.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

protocol SignUpStepCoordinatorResultDelegate: class {
    func signUpCoordinatorDidCompleteStep(_ coordinator: Coordinator)
}

class SelectGenderStepCoordinator: BaseCoordinator {
    
    weak var resultDelegate: SignUpStepCoordinatorResultDelegate?

    override func instantiateViewController() -> UIViewController? {
        let selectGenderStepViewController = SelectGenderStepViewController()
        let selectGenderStepViewModel = SelectGenderStepViewModelImpl(profileBuilder: context.profileBuilder)
        selectGenderStepViewController.viewModel = selectGenderStepViewModel
        selectGenderStepViewModel.coordinatorDelegate = self
        
        return selectGenderStepViewController
    }
}

extension SelectGenderStepCoordinator: SelectGenderStepViewModelCoordinatorDelegate {
    func viewModelDidSelectGender() {
        resultDelegate?.signUpCoordinatorDidCompleteStep(self)
    }
}

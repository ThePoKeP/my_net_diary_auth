//
//  SelectGenderStepViewController.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import UIKit

class SelectGenderStepViewController: BaseViewController {
    
    var viewModel: SelectGenderStepViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Get Started"
    }
    
    @IBAction private func maleButtonDidTap(sender: UIButton) {
        viewModel?.selectGender(.male)
    }
    
    @IBAction private func femaleButtonDidTap(sender: UIButton) {
        viewModel?.selectGender(.female)
    }
}

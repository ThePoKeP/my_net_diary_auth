//
//  SelectGenderStepViewModel.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

protocol SelectGenderStepViewModelCoordinatorDelegate: class {
    func viewModelDidSelectGender()
}

protocol SelectGenderStepViewModel: ViewModel {
    func selectGender(_ gender: Gender)
}

class SelectGenderStepViewModelImpl: SelectGenderStepViewModel {
    
    weak var coordinatorDelegate: SelectGenderStepViewModelCoordinatorDelegate?
    
    private let profileBuilder: ProfileBuilder
    
    init(profileBuilder: ProfileBuilder) {
        self.profileBuilder = profileBuilder
    }
    
    func selectGender(_ gender: Gender) {
        profileBuilder.fillGender(gender)
        coordinatorDelegate?.viewModelDidSelectGender()
    }
}

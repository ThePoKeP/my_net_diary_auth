//
//  AppContext.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/29/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

public protocol AppContext {
    var profileBuilder: ProfileBuilder { get }
    var healthStoreService: HealthStoreService { get }
}

class AppContextImpl: AppContext {
    
    let profileBuilder: ProfileBuilder = ProfileBuilderImpl()
    let healthStoreService: HealthStoreService = HealthStoreServiceImpl()
}

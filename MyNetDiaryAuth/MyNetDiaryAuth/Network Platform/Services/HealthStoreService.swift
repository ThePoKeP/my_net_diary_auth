//
//  HealthStoreService.swift
//  MyNetDiaryAuth
//
//  Created by For Tests on 9/28/20.
//  Copyright © 2020 alexey.puchko. All rights reserved.
//

import HealthKit

public enum RequestResult<T> {
    case success(T)
    case fail(Error)
}

public enum HealthStoreServiceError: Error {
    case empty
}

public protocol HealthStoreService {
    func requestAuthorization(resultHandler: @escaping (RequestResult<Bool>) -> Void)
    func requestBirthday(resultHandler: @escaping (RequestResult<Date?>) -> Void)
    func requestCurrentWeight(resultHandler: @escaping (RequestResult<BodyWeight>) -> Void)
    func requestCurrentHeight(resultHandler: @escaping (RequestResult<BodyHeight>) -> Void)
}

class HealthStoreServiceImpl: HealthStoreService {
    
    let healthKitStore: HKHealthStore
    
    private var bodyWeightType: HKSampleType?
    
    private var bodyHeightType: HKSampleType?
    
    private lazy var readSet: Set<HKObjectType> = {
        var readSet: Set<HKObjectType> = []
        if let bodyMassType = $0.bodyWeightType {
            readSet.insert(bodyMassType)
        }
               
        if let bodyHeightType = $0.bodyHeightType {
            readSet.insert(bodyHeightType)
        }
        
        if let birthdayType = HKCharacteristicType.characteristicType(forIdentifier: .dateOfBirth) {
            readSet.insert(birthdayType)
        }
        return readSet
    }(self)
    
    init() {
        healthKitStore = HKHealthStore()
        bodyWeightType = HKSampleType.quantityType(forIdentifier: .bodyMass)
        bodyHeightType = HKSampleType.quantityType(forIdentifier: .height)
    }
    
    func getRequestStatusForAuthorization(resultHandler: @escaping (RequestResult<Bool>) -> Void) {
        guard readSet.count > 0 else {
            resultHandler(.fail(HealthStoreServiceError.empty))
            return
        }
        
        healthKitStore.getRequestStatusForAuthorization(toShare: [], read: readSet) { (status, error) in
            DispatchQueue.main.async {
                guard let error = error else {
                    resultHandler(.success(status == .unnecessary))
                    return
                }

                resultHandler(.fail(error))
            }
        }
    }
    
    func requestAuthorization(resultHandler: @escaping (RequestResult<Bool>) -> Void) {
        
        guard readSet.count > 0 else {
            resultHandler(.fail(HealthStoreServiceError.empty))
            return
        }
        
        getRequestStatusForAuthorization { result in
            
            switch result {
            case .success(let status):
                if !status {
                    self.healthKitStore.requestAuthorization(toShare: nil, read: self.readSet) { (success, error) in
                        DispatchQueue.main.async {
                            guard let error = error else {
                                resultHandler(.success(success))
                                return
                            }
                            resultHandler(.fail(error))
                        }
                    }
                    
                } else {
                    resultHandler(.success(true))
                }
                
            case .fail(let error):
                resultHandler(.fail(error))
            }
        }
    }
    
    func requestBirthday(resultHandler: @escaping (RequestResult<Date?>) -> Void) {
        
        do {
            let dateComponents = try healthKitStore.dateOfBirthComponents()
            var calendar = Calendar(identifier: .gregorian)
            calendar.timeZone = TimeZone(secondsFromGMT: 0)!
            let birthday = calendar.date(from: dateComponents)
            resultHandler(.success(birthday))
            
        } catch let error {
            resultHandler(.fail(error))
        }
    }
    
    func requestCurrentHeight(resultHandler: @escaping (RequestResult<BodyHeight>) -> Void) {
        requestCurrentBodyParameter(with: bodyHeightType) { result in
            switch result {
            case .success(let sample):
                let bodyHeightMeter = sample.quantity.doubleValue(for: HKUnit.meter())
                resultHandler(.success(BodyHeight(with: bodyHeightMeter, unitType: .meter)))
                
            case .fail(let error):
                resultHandler(.fail(error))
            }
        }
    }
    
    func requestCurrentWeight(resultHandler: @escaping (RequestResult<BodyWeight>) -> Void) {
        requestCurrentBodyParameter(with: bodyWeightType) { result in
            switch result {
            case .success(let sample):
                let bodyWeightKg = sample.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
                resultHandler(.success(BodyWeight(with: bodyWeightKg, unitType: .kg)))
                
            case .fail(let error):
                resultHandler(.fail(error))
            }
        }
    }

    private func requestCurrentBodyParameter(with sampleType: HKSampleType?,
                                             resultHandler: @escaping (RequestResult<HKQuantitySample>) -> Void) {
        
        guard let sampleType = sampleType else {
            resultHandler(.fail(HealthStoreServiceError.empty))
            return
        }
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        let query = HKSampleQuery(sampleType: sampleType,
                                  predicate: nil,
                                  limit: 1,
                                  sortDescriptors: [sortDescriptor]) { (query, results, error) in
            DispatchQueue.main.async {
                if let result = results?.first as? HKQuantitySample {
                    resultHandler(.success(result))
                    return
                }
            
                resultHandler(.fail(HealthStoreServiceError.empty))
            }
        }
        healthKitStore.execute(query)
    }
}
